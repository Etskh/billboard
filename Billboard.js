


var Billboard = (function(document){
    var exports = {

        // Number of divisions in the billboard
        divisions: 41,

        // how many seconds it takes for the effect to
        // ripple all the way down the divisions
        lag: 1,

        // The duration of the resting billboard in seconds
        duration: 4,


        Load : function(config) {

            // Make sure we have a config
            if( config === undefined ) {
                console.error("Billboard.Load needs a JSON config initialiser");
                return false;
            }

            // Load the background
            if( config.background === undefined ) {
                console.error("background needed in config");
                return false;
            }

            // Load the images
            if( config.images === undefined ) {
                console.error("images needed in config");
                return false;
            }
            var images = config.images;

            // Find out if we're doing fullscreen or not
            if( !config.fullscreen ) {
              config.fullscreen = false;
            }

            // Number of divisions in the billboard
            var divisions = config.divisions?config.divisions:Billboard.divisions;
            // The duration of the resting billboard in seconds
            var duration = config.duration?config.duration:Billboard.duration;
            // how many seconds it takes for the effect to
            // ripple all the way down the divisions
            var lag = config.lag?config.lag:Billboard.lag;

            var _billboardElem = document.getElementById('billboard');
            var _backgroundElem = document.getElementById('background');
            if( ! _billboardElem ) {
                var _billboardElem = document.createElement('div');
                var _backgroundElem = document.createElement('div');
                _billboardElem.id = 'billboard';
                _backgroundElem.id = 'background'
                _backgroundElem.appendChild(_billboardElem);
                document.body.appendChild(_backgroundElem);
            }

            var billboardDim = [
                config.botRight[0] - config.topLeft[0],
                config.botRight[1] - config.topLeft[1]
            ];
            if( config.fullscreen ) {
              var aspectRatio = billboardDim[1] / billboardDim[0];
              _billboardElem.style.left = '0px';
              _billboardElem.style.top = '0px';
              _billboardElem.style.width = '100%';
              _billboardElem.style.height = (100*aspectRatio)+'%';

              adjustDivHeight(_backgroundElem, aspectRatio*0.80);
            }
            else {
              if( config.topLeft && config.botRight ) {
                  _billboardElem.style.left = (100*config.topLeft[0])+'%';
                  _billboardElem.style.top = (100*config.topLeft[1])+'%';

                  var billboardDim = [
                      config.botRight[0] - config.topLeft[0],
                      config.botRight[1] - config.topLeft[1]
                  ];

                  _billboardElem.style.width = (100*billboardDim[0])+'%';
                  _billboardElem.style.height = (100*billboardDim[1])+'%';
              }

              LoadBackground( _backgroundElem, config.background );
            }





            var _divisionSize = 100/divisions;
            var _imageIndex = 1;

            function Spin() {
                $('.slat').each(function(){
                    var rotY = parseInt(this.dataset.index);
                    $(this).css('transform','perspective(500px) translateZ(20px) rotateY('+rotY+'deg)');
                    //$(this).css('transform','rotateY('+rotY+'deg)');
                    rotY += (360/images.length);
                    this.dataset.index = rotY;
                });
                _imageIndex++;
            }

            for( var i=images.length-1; i>-1; --i ) {
                for( var d=0; d<divisions; ++d ) {
                    var slatElem = document.createElement('div');
                    slatElem.className = "slat img"+i+" col"+d;
                    slatElem.dataset.index = i*(360/images.length);
                    _billboardElem.appendChild(slatElem);
                }
                $('.img'+i).css('backgroundImage',"url('"+images[i]+"')");
            }


            for( var d=0; d<divisions; ++d ) {
                var $col=$('.col'+d);
                $col.css('left', (_divisionSize*d)+'%');
                $col.css('backgroundPosition',(d*_divisionSize)+"% top");
                $col.css('transform','perspective(500px) translateZ(20px) rotateY(0deg)');
                $col.css('transitionDelay', ((d/divisions)*lag)+'s');
            }

            $('.slat').css('width', _divisionSize+'%');

            setInterval(Spin,duration*1000);

            return true;
        }
    };


    function adjustDivHeight( elem, aspectRatio ) {
        setTimeout(function() {
            $(elem).height(($(elem).width() * aspectRatio)+'px');
        }, 300);
    }

    function LoadBackground( backgroundElem, path ) {
        var backgroundImage = new Image();
        backgroundImage.onload = function() {
            backgroundElem.style.backgroundImage = 'url("'+path+'")';
            var width = backgroundImage.width;
            var height = backgroundImage.height;
            adjustDivHeight(backgroundElem, height / width);
            $(window).resize(function() {
                adjustDivHeight(backgroundElem, height / width );
            });
        };
        backgroundImage.src = path;
    }

    // For debugging
    $(window).click(function(e){
        var bgHeight = $('#background').height();
        var left = e.clientX / $(document).width();
        var top = e.clientY / bgHeight;
        //console.log(bgHeight);
        console.log([left,top]);
    });

    return exports;
})(document);
