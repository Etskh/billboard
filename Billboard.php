<?php
session_start();




class Tools {
	static public function generateHash() {
		$str = base64_encode( openssl_random_pseudo_bytes(24));
		$str = str_replace( '/', '_', $str);
		$str = str_replace( '+', '-', $str);
		return $str;
	}

	static public function uploadImage( $file ) {

		// Check size
		if ($file["size"] > 500000) {
			die('size is too beeeg');
			return false;
		}


		// Check file-type
		$imageFileType = pathinfo( basename($file["name"]), PATHINFO_EXTENSION);
		if( $imageFileType != "jpg" &&
			$imageFileType != "png" &&
			$imageFileType != "jpeg" &&
			$imageFileType != "gif" ) {

			die('not the right eeextension (was a '. $imageFileType .')');
			return false;
		}


		$target_file = 'upload/' . Tools::generateHash() . $imageFileType;
		if( !move_uploaded_file($file["tmp_name"], $target_file)) {
			die('didn\'t mooooove');
			return false;
		}

		// Amazing it worked!
		return $target_file;
	}
}



class Set
{
	static public $DIR = 'sets';

	static public function getAll() {
		$sets = array();
		if( !file_exists(Set::$DIR) ) {
			mkdir(Set::$DIR);
		}
		$files = scandir(Set::$DIR);
		foreach( $files as $file ) {
			if( $file[0] == '.' ) {
				continue;
			}
			// Add it to the sets array
			$hash = array_shift(explode(".",$file));
			$set = new Set($hash);
			$set->data = json_decode( file_get_contents( Set::$DIR.'/'.$file ) );
			$set->name = isset($set->data->name) ? $set->data->name: 'Untitled';
			$sets[] = $set;
		}
		return $sets;
	}

	static public function createNew() {

		$set = new Set(Tools::generateHash() );

		$set->name = "Untitled";
		$set->data = array(
			"name" => "Untitled",
			"background"=> "bg/van.jpg",
			"topLeft"=> array( 0.24811844589688, 0.38435989717224 ),
			"botRight"=> array( 0.60527756653992, 0.63927756653992 ),
			"divisions"=> 41,
			"fullscreen"=> true,
			"images" => array(),
		    "duration"=> 8,
		    "lag"=> 4
		);

		$set->save();

		return $set;
	}

	static public function getSetByHash( $hash ) {
		$allSets = Set::getAll();
		foreach( $allSets as $set ) {
			if( $set->hash == $hash ) {
				return $set;
			}
		}
		return false;
	}

	public function addImage( $image ) {
		$this->data->images[] = $image;
	}

	public function Set ($hash) {
		$this->hash = $hash;
	}

	public function setName($name) {
		$this->name = $name;
		$this->data->name = $name;
	}

	public function getName() {
		return $this->name;
	}

	public function getHash() {
		return $this->hash;
	}

	public function save() {
		file_put_contents(Set::$DIR.'/'.$this->hash.'.json', json_encode($this->data, JSON_PRETTY_PRINT));
	}


	public function delete() {
		unlink(Set::$DIR.'/'.$this->hash.'.json');
	}

	public function loadJavascript() {
		?>
		Billboard.Load(<?php print(json_encode($this->data)); ?>);
		<?php
	}

	public $name = 'asdf';
	public $data = array();
	private $hash = 'asdf';
}




// If the URL has a set parameter, load in the set - otherwise,
// Load a password entry
//
class Page {
	public function Page() {
		$this->page = array_pop(explode("/", __FILE__));

		if( isset($_GET['set'])) {
			$this->hash = $_GET['set'];
		}
	}

	public function getSet() {
		return $this->hash;
	}

	public function setSet($hashOrSet) {
		$this->hash = $this->getHashFromHashOrSet($hashOrSet);
		return $this;
	}

	private function getHashFromHashOrSet($hashOrSet) {

		$hash = $hashOrSet;
		if( !$hashOrSet ) {
			return false;
		}

		if( !is_string($hashOrSet) ) {
			$hash = $hashOrSet->getHash();
		}

		return $hash;
	}

	public function reload() {
		header('Location: '.$this->toString());
		exit;
	}

	public function toString($hashOrSet=false) {

		$params = array();
		if( $hashOrSet ) {
			$params[] = 'set='.$this->getHashFromHashOrSet($hashOrSet);
		}
		else if( $this->hash ) {
			$params[] = 'set='.$this->hash;
		}
		if( $this->action ) {
			$params[] = 'action='.$this->action;
		}

		return $this->page.'?'.implode("&",$params);
	}

	public $page = '';
	public $hash = null;
	public $action = null;

}



// Creates the page and its state
$page = new Page();



// Check if the user logged in
//
if( isset($_POST['password'])) {
	if( $_POST['password'] == "asdf" ) {
		$_SESSION['isAuthenticated'] = true;
	}
}
if( isset($_POST['action']) ) {
	if( $_SESSION['isAuthenticated'] !== true ) {
		die('What do we say to post requests without being logged in? "Not today"');
	}

	switch($_POST['action']) {

	// Log the user out
	case 'logout':
		$_SESSION['isAuthenticated'] = false;
		break;

	// Creates a new set and reloads the page with it
	case 'create':
		$page->setSet( Set::createNew() );
		$page->reload();
		break;

	// Saves the data of a set
	case 'edit':
		$set = Set::getSetByHash( $page->getSet() );
		$set->setName($_POST['name']);
		$set->save();
		unset($set);
		break;

	case 'delete':
		$set = Set::getSetByHash( $page->getSet() );
		$set->delete();
		$page->setSet(false);
		$page->reload();
		break;

	case 'upload-image':
		var_dump($_FILES);
		die();
		$target_file = Tools::uploadImage($_FILES["image"]);
		$set = Set::getSetByHash( $page->getSet() );
		$set->addImage($target_file);
		$set->save();
		unset($set);
		break;

	}
}

// Set management
$uploadDir = 'upload';
$bgDir = 'bg';
if( !file_exists($uploadDir) ) {
	mkdir($uploadDir);
}
if( !file_exists($bgDir) ) {
	mkdir($bgDir);
}



$set = Set::getSetByHash($page->getSet());






?>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.3/normalize.min.css" />
  		<link rel="stylesheet" type="text/css" href="Billboard.css" />
		<style>

			#connect {
				z-index: 100;
				position:absolute;
				right:1em;
				top:1em;
			}

			#password-entry {
				padding:1em;
				text-shadow:#FFF 0px 0px 3px;
				background: #63a6e9;
				box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 20px 20px;
				border-radius: 0.3em;
			}

			#set-list {
				z-index: 100;
				position:absolute;
				top:3em;
				right:1em;
			}

			#set-info {
				z-index: 100;
				position:absolute;
				top:1em;
				left:1em;
			}

			.image-in-set {
				margin:10px;
				width:100px;
				height:50px;
				background-size:contain;
				position:relative;
				box-shadow: #333 0px 0px 5px;
			}

			.image-in-set .remove {
				position: absolute;
				right: 0px;
				color: #EEE;
				cursor:pointer;
				padding: 5px;
				text-shadow: #000 0px 0px 3px;
				font-weight: bold;
				font-size:120%;
				top: 8px;
			}
			.image-in-set .remove:hover {
				color: #FFF;
			}

		</style>
	</head>
	<body>
		<?php
		// This is where editing exists
		if( $_SESSION['isAuthenticated'] == true ) {

			?>
			<div id="connect">
				<form name="pass" action="<?php print($page->toString()); ?>" method="POST">
					<button type="submit" name="action" value="logout">Logout</button>
				</form>
			</div>

			<div id="set-list"><?php
					$sets = Set::getAll();
					foreach( $sets as $s ) {
						?><a href="<?=$page->toString($s)?>"><?=$s->getName();?></a><br/><?php
					}
				?>
				<form name="create-set" method="POST" action="<?php print($page->toString()); ?>">
					<button name="action" value="create">+ Create New</button>
				</form>
			</div><?php
			if( $set ) { ?>
				<div id="set-info">
					<?php /* This is the set-editor */ ?>
					<div id="set-data">
						<form name="edit-set" method="POST" action="<?php print($page->toString()); ?>">
							<input type="text" name="name" value="<?= $set->getName(); ?>" />
							<button type="submit" name="action" value="edit">Save Changes</button>
						</form>
						<form name="delete-set" method="POST" action="<?php print($page->toString()); ?>">
							<button type="submit" name="action" value="delete">Delete Set</button>
						</form>
					</div>
					<?php /* This is the set-editor */ ?>
					<div id="set-images">
						<div id="image-list">
						<?php
							foreach( $set->data->images as $img ) {
								?><div class="image-in-set" style="background-image:url(<?= $img ?>)">
									<div class="remove">X</div>
								</div><?php
							}
						?>
						</div>
						<form name="upload-image" method="POST" action="<?php print($page->toString()); ?>">
							<input type="file" name="image"/><br/>
							<button name="action" value="upload-image" type="submit">Upload</button>
						</form>
					</div>
				</div>
				<?php
			}
		} // is Authenticated
		else {

			?><div id="connect">
				<button type="button" id="edit-set">Pencil</button>
				<div id="password-entry" style="display:none;">
					<form name="pass" action="<?php print($page->toString()); ?>" method="POST">
						<input id="password" placeholder="Enter the password" name="password" type="password"/>
						<button type="submit">Enter</button>
						<button type="button" id="nevermind">Cancel</button>
					</form>
				</div>
			</div>
			<?php
		}
		?>
		<div id="email-link">
			<a href="mailto:email@example.com"></a>
		</div>
		<div id="set-error"><?php
		if( !$set ) {
			?><h1>Unknown billboard set</h1><?php
		}
		?></div>
		<div id="scripts">
			<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
	    <script type="text/javascript" src="Billboard.js"></script>
			<script>
				$(document).ready(function() {
					$('#edit-set').click(function(){
						$('#edit-set').hide();
						$('#password-entry').fadeIn(100);
						$('#password').focus();
					});
					$('#nevermind').click(function(){
						$('#password-entry').fadeOut(100, function(){
							$('#edit-set').show();
						});
					})
					<?php
					if( $set ) {
						$set->loadJavascript();
					}
					?>
				});
			</script>
		</div>
	</body>
</html>
